#include "GPIO.h"

#ifndef MAX_H_
#define MAX_H_

using namespace exploringBB;

	class MAX {
	
		private:
			GPIO dataPin;
			GPIO clkPin;
			GPIO ltchPin;

			int LSBFIRST;
			int nDevices;
			int seg_number;
		public:
			MAX(int number);
			MAX(int data, int clk, int latch, int segments);
			virtual int getNumber();
			virtual void shiftOut(int val);
		}; //MAX


#endif /* MAX_H_ */
