#include "player.h"
#include "GPIO.h"
#include <iostream>
using namespace  std;
using namespace exploringBB;


Player::Player(int bit1, int bit2, int bit3, int bit4, int bit5, int bit6, int bit7, int bit8,  int pNumber) {
	BIT2.init(60);
	BIT2.setDirection(INPUT);

	playerNumber = pNumber;
}

int Player::getPlayerNumber(){
	return(this->playerNumber);
}

void Player::welcomeMsg(){
	cout << "Player " << this->getPlayerNumber() << " joined the game" << endl; 
}
