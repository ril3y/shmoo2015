#include<iostream>
#include "GPIO.h"
#include "max.h"
#include "player.h"

using namespace std;

#define CLK_PIN 3
#define DATA_PIN 2
#define LATCH_PIN 49
#define SEGS 1
#define PLAYER1 1
#define PLAYER2 2


int main() {
	cout << "=================" << endl;
	cout << "ShmooDeck Stated!" << endl;
	cout << "=================" << endl;
	Player p1(30, 60, 31, 50, 48, 51,  5,  4,  1);
	Player p2(66, 67, 69, 68, 45, 44, 23, 26,  2);//P8_07, P8_08, P8_09, P8_10, P8_11, P8_12, P8_13 P


	p1.welcomeMsg();
	//p2.welcomeMsg();

	MAX max_chip(CLK_PIN, DATA_PIN, LATCH_PIN, SEGS);
	cout << "\tMAX7219's is : " << max_chip.getNumber() << endl; 
	//max_chip.shiftOut(255);
}

