#include "GPIO.h"

#ifndef PLAYER_H_
#define PLAYER_H_

using namespace exploringBB;

	class Player {
	
		private:
			int playerNumber;
			GPIO BIT1;
			GPIO BIT2;
			GPIO BIT3;

		public:
			Player(int bit1, int bit2, int bit3, int bit4, int bit5, int bit6, int bit7, int bit8, int number);

			virtual void welcomeMsg();
			virtual int getPlayerNumber();
		}; //Player


#endif /* PLAYER_H_ */
